/*Créez une classe Produit :

nom
prix

Créez une classe Panier avec :

une méthode ajoute( produit )
une méthode retire( produit )
une proprieté totalHT
une proprieté totalTTC

Bonus : reprendre cet exemple en ajoutant une classe Viennoiserie qui hérite 
de la classe Produit en lui ajoutant l'attribut booléen frais.*/


var Produit = function(nom, prix) {
	this.nom = nom;
	this.prix = prix;
}

var baguette = new Produit('Baguette', 0.85); // prix HT
var croissant = new Produit('Croissant', 0.80);



var Panier = function() {
	this.nbrPanier = 0;
	this.totalHT = 0;
	this.totalTTC = 0;
	this.ajoute = function(Produit) {
		this.nbrPanier ++;
		this.totalHT = this.totalHT + Produit.prix;
		this.totalTTC = (((5.5 / 100) * this.totalHT) + this.totalHT);
	}
		
	this.retire = function(Produit) {
		this.nbrPanier --;
		this.totalHT = this.totalHT - Produit.prix;
		this.totalTTC = (((5.5 / 100) * this.totalHT) + this.totalHT);
	}
}


var panier = new Panier();
panier.ajoute(baguette);
panier.ajoute(baguette);
panier.ajoute(croissant);
panier.retire(croissant);



console.log("Prix HT : " + panier.totalHT);
console.log("Prix TTC : " + panier.totalTTC);
