var Vehicule = function(poids, vitesse) {
    this.poids = poids;
    this.vitesse = vitesse;
    this.avance = function() {
        console.log("vroum vroum");
    }
}

var titine = new Vehicule("1000", "50");
titine.poids;
titine.vitesse;
titine.avance();

Vehicule.prototype.klaxonne = function() {
    console.log("Pouet")
};

titine.klaxonne();

var porsche = new Vehicule();
porsche.avance = function() {
    console.log("Vrouuuuuum !");
};

titine.avance();
porsche.avance();

Voiture.prototype = new Vehicule();
Voiture.prototype.constructor = Voiture;
function Voiture(poids, vitesse, nombre_passagers) {
    this.nombre_passagers = nombre_passagers;
    this.allume_autoradio = function() {
        console.log("lalala");
    }
}

Voiture.prototype.freiner = function() {
    console.log("gniiii");
}

Voiture.prototype.avance = function() {
    console.log("vroum");
}

var coccinelle = new Voiture(500, 50, 4);
coccinelle.poids;
coccinelle.vitesse;
coccinelle.nombre_passagers;
coccinelle.avance();
coccinelle.allume_autoradio();
coccinelle.freiner();

/*var trotinette = new Vehicule(5, 20);
trotinette.avance();
trotinette.allume_autoradio();*/

var Camion = new Voiture(3500, 90, 3);
Camion.poids;
Camion.vitesse;
Camion.nombre_passagers;
Camion.avance();
Camion.allume_autoradio();
Camion.freiner();