/*Créez une classe Chrono avec :


une méthode start()


une méthode pause()


une méthode stop()


une propriété currentTime*/

let Compteur = document.getElementById("secondes");
let btn_start = document.getElementById("start");
let btn_pause = document.getElementById("pause");
let btn_stop = document.getElementById("stop");

function ajout() {
	Compteur.innerHTML = +(Compteur.textContent) + 1;
}

let Chrono = function () {

	this.compteur = 0;
	this.intervalId;

	this.start = function () {
		this.intervalId = setInterval(ajout, 1000)
	}
	this.pause = function () {
		clearTimeout(Compteur);
	}
	this.stop = function () {
		this.compteur = 0;
	}
	/*this.currentTime = currentTime;*/
}

let chrono = new Chrono();

Compteur.innerHTML = chrono.compteur;

btn_start.addEventListener("click", function () {
	chrono.start();
});

btn_pause.addEventListener("click", function () {
	chrono.pause();
});

btn_stop.addEventListener("click", function () {
	chrono.stop();
});
